import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { LoaderComponent } from './components/loader/loader.component';

@NgModule({
    imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule
    ],
    exports: [
        ReactiveFormsModule,
        FormsModule,
        LoaderComponent
    ],
    declarations: [LoaderComponent]
})
export class SharedModule {}