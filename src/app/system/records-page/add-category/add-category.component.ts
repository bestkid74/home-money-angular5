import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {NgForm} from '@angular/forms';

import {Category} from '../../shared/models/category.model';
import {CategoriesService} from '../../shared/services/categories.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.sass']
})
export class AddCategoryComponent implements OnDestroy {
    sub1: Subscription;
  @Output() onCategoryAdd = new EventEmitter<Category>();

  constructor(private categoriesService: CategoriesService ) { }

  onSubmit(form: NgForm) {
    const {name} = form.value;
    let {capacity} = form.value;
    if (capacity < 1) {
        capacity = 1;
    }

    const category = new Category(name, capacity);

    this.sub1 = this.categoriesService.addCategory(category)
        .subscribe((respCategory: Category) => {
          form.reset();
          form.form.patchValue({opacity: 1});
          this.onCategoryAdd.emit(respCategory);
        });
  }

  ngOnDestroy() {
      if (this.sub1) {this.sub1.unsubscribe();}
  }
}
