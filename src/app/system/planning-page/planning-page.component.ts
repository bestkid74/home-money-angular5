import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import {Bill} from '../shared/models/bill.model';
import {BillService} from '../shared/services/bill.service';
import {Category} from '../shared/models/category.model';
import {CategoriesService} from '../shared/services/categories.service';
import {EventsService} from '../shared/services/events.service';
import {AppEvent} from '../shared/models/event.model';

@Component({
  selector: 'app-planning-page',
  templateUrl: './planning-page.component.html',
  styleUrls: ['./planning-page.component.sass']
})
export class PlanningPageComponent implements OnInit, OnDestroy {
  sub1: Subscription;

  isLoaded = false;

  bill: Bill;
  categories: Category[] = [];
  appEvents: AppEvent[] = [];

  constructor(
      private billService: BillService,
      private categoriesService: CategoriesService,
      private eventService: EventsService
  ) { }

  ngOnInit() {
      this.sub1 = Observable.combineLatest(
          this.billService.getBill(),
          this.categoriesService.getCategories(),
          this.eventService.getEvents()
      ).subscribe((data: [Bill, Category[], AppEvent[]]) => {
          this.bill = data[0];
          this.categories = data[1];
          this.appEvents = data[2];

          this.isLoaded = true;
      });
  }

  getCategoryCost(cat: Category): number {
      const catEvents = this.appEvents.filter(ev => cat.id === ev.category && ev.type === 'outcome');

    return catEvents.reduce((total, ev) => {
        total += ev.amount;
        return total;
    }, 0);
  }

  private getPercent(cat: Category): number {
      const percent = this.getCategoryCost(cat) / cat.capacity * 100;
      return percent > 100 ? 100 : percent;
  }

  getCatPercent(cat: Category): string {
      return this.getPercent(cat) + '%';
  }

  getCatColorClass(cat: Category): string {
      const percent = this.getPercent(cat);
      return percent < 50 ? 'success' : percent >= 100 ? 'danger' : 'warning';
  }

  ngOnDestroy() {
      if (this.sub1) { this.sub1.unsubscribe(); }
  }

}
