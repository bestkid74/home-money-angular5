import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import * as moment from 'moment';

import {CategoriesService} from '../shared/services/categories.service';
import {EventsService} from '../shared/services/events.service';
import {Category} from '../shared/models/category.model';
import {AppEvent} from '../shared/models/event.model';


@Component({
  selector: 'app-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.sass']
})
export class HistoryPageComponent implements OnInit, OnDestroy {
  categories: Category[] = [];
  appEvents: AppEvent[] = [];
  filteredEvents: AppEvent[] = [];
  chartData = {
    labels: [],
    datasets: [
        {
          data: [],
          backgroundColor: [
              'rgba(255, 20, 147, .3)', 'rgba(107, 142, 35, .3)', 'rgba(32, 178, 170, .3)', 'Magenta', 'IndianRed',
              'LimeGreen', 'FireBrick', 'Tomato', 'Cyan', 'DarkKhaki', 'Aquamarine',
              'SteelBlue', 'Plum', 'RoyalBlue', 'DarkOrchid'
          ],
          hoverBackgroundColor: [
              'rgba(255, 20, 147, 1)', 'rgba(107, 142, 35, 1)', 'rgba(32, 178, 170, 1)', 'Magenta', 'IndianRed',
              'LimeGreen', 'FireBrick', 'Tomato', 'Cyan', 'DarkKhaki', 'Aquamarine',
              'SteelBlue', 'Plum', 'RoyalBlue', 'DarkOrchid'
          ]
        }
    ]
  };
  s1: Subscription;
  isLoaded = false;
  isFilterVisible = false;
  changesFlag = false;

  constructor(
      private categoriesService: CategoriesService,
      private eventService: EventsService
  ) {}

  ngOnInit() {
    this.s1 = Observable.combineLatest(
        this.categoriesService.getCategories(),
        this.eventService.getEvents()
    ).subscribe((data: [Category[], AppEvent[]]) => {
      this.categories = data[0];
      this.appEvents = data[1];
      this.setOriginalEvents();
      this.calculateChartData();
      this.isLoaded = true;
    });
  }

  setOriginalEvents() {
      this.filteredEvents = this.appEvents.slice();
  }

  calculateChartData() {
    this.chartData.labels = [];
    this.chartData.datasets[0].data = [];

    this.categories.forEach((cat, index) => {
      const catEvent = this.filteredEvents.filter((e) => e.category === cat.id && e.type === 'outcome');
      const value = catEvent.reduce((total, e) => {
        total += e.amount;
        return total;
      }, 0);

      this.chartData.labels.push(cat.name);
      this.chartData.datasets[0].data.push(value);
    });
  }

  private toggleFilterVisibility(dir: boolean) {
    this.isFilterVisible = dir;
  }

  openFilter() {
    this.toggleFilterVisibility(true);
  }

  onFilterApply(filterData) {
    this.toggleFilterVisibility(false);
    this.setOriginalEvents();

    const startPeriod = moment().startOf(filterData.period).startOf('d');
    const endPeriod = moment().endOf(filterData.period).endOf('d');

    this.filteredEvents = this.filteredEvents
        .filter((ev) => {
            return filterData.types.indexOf(ev.type) !== -1;
        })
        .filter((ev) => {
            return filterData.categories.indexOf(ev.category.toString()) !== -1;
        })
        .filter((ev) => {
            const momentDate = moment(ev.date, 'DD.MM.YYYY HH:mm:ss');
            return momentDate.isBetween(startPeriod, endPeriod);
        });

    this.calculateChartData();
    this.changesFlag = !this.changesFlag;
  }

  onFilterCancel() {
      this.toggleFilterVisibility(false);
      this.setOriginalEvents();
      this.calculateChartData();
      this.changesFlag = !this.changesFlag;
  }

  ngOnDestroy() {
    if (this.s1) {
      this.s1.unsubscribe();
    }
  }
}
