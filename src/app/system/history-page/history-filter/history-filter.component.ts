import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Category} from '../../shared/models/category.model';

@Component({
  selector: 'app-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.sass']
})
export class HistoryFilterComponent {
    @Output() onFilterCancel = new EventEmitter();
    @Output() onFilterApply = new EventEmitter();

    @Input() categories: Category[] = [];

    selectedPeriod = 'd';
    selectedTypes = [];
    selectedCategories = [];

    timePeriod = [
        {type: 'd', label: 'День'},
        {type: 'w', label: 'Неделя'},
        {type: 'M', label: 'Месяц'},
    ];

    types = [
        {type: 'income', label: 'Доход'},
        {type: 'outcome', label: 'Расход'},
    ];

  private calculateInputParams(field: string, checked: boolean, value: string) {
      if (checked) {
          this[field].indexOf(value) === -1 ? this[field].push(value) : null;
      } else {
          this[field] = this[field].filter(item => item !== value);
      }
  }

  handleChangeType({checked, value}) {
      this.calculateInputParams('selectedTypes', checked, value);
  }
  handleChangeCategory({checked, value}) {
      this.calculateInputParams('selectedCategories', checked, value);
  }

  applyFilter() {
      this.onFilterApply.emit({
          types: this.selectedTypes,
          categories: this.selectedCategories,
          period: this.selectedPeriod
      });
  }

  closeFilter() {
      this.selectedPeriod = 'd';
      this.selectedTypes = [];
      this.selectedCategories = [];
      this.onFilterCancel.emit();
  }
}
