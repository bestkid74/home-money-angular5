import {Component, Input, OnChanges, ViewChild} from '@angular/core';
import { ChartComponent } from 'angular2-chartjs';

@Component({
  selector: 'app-history-chart',
  templateUrl: './history-chart.component.html',
  styleUrls: ['./history-chart.component.sass']
})
export class HistoryChartComponent implements OnChanges {
    @Input() someData;
    @Input() changesFlag;
    @ViewChild(ChartComponent) chart: ChartComponent;
    options = {
        responsive: true,
        maintainAspectRatio: true,
        legend: false,
        layout: {
            padding: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 50
            }
        }
    };

    ngOnChanges(values) {
        if (values.changesFlag && !values.changesFlag.firstChange) {
            this.chart.chart.update();
        }
    }
}
