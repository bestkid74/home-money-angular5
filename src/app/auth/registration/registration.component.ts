import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {Message} from '../../shared/models/message.model';
import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/models/user.model';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {

    form: FormGroup;
    message: Message;

  constructor(
      private userService: UsersService,
      private router: Router,
      private title: Title,
      private meta: Meta
  ) {
      title.setTitle('Регистрация в системе');
      meta.addTags([
          {name: 'keywords', content: 'регистрация'},
          {name: 'description', content: 'Страница регистрации в системе'}
      ]);
  }

    private showMessage(text: string, type: string = 'danger') {
        this.message = new Message(type, text);

        window.setTimeout(() => {
            this.message.text = '';
        }, 5000);
    }

  ngOnInit() {
      this.message = new Message('danger', '');
      this.form = new FormGroup({
          'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails.bind(this)),
          'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
          'name': new FormControl(null, [Validators.required, Validators.minLength(3)]),
          'agree': new FormControl(false, [Validators.requiredTrue])
      });
  }

    onSubmit() {
      const {email, password, name} = this.form.value;
      const user = new User(email, password, name);
      this.userService.createNewUser(user)
          .subscribe((newUser: User) => {
              this.router.navigate(['/login'], {queryParams: {nowCanLogin: true}});
          })
      ;
    }

    forbiddenEmails(control: FormControl): Promise<any> {
      return new Promise((resolve, reject) => {
          this.userService.getUserByEmail(control.value)
              .subscribe((user: User) => {
                  if (user[0]) {
                      resolve({forbiddenEmail: true});
                  } else {
                      resolve(null);
                  }
              })
          ;
      });
    }
}
