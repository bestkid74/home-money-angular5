import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/models/user.model';
import {Message} from '../../shared/models/message.model';
import {AuthService} from '../../shared/services/auth.service';
import {fadeStateTrigger} from '../../shared/components/animations/fade.animation';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
  animations: [fadeStateTrigger]
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: Message;

  constructor(
      private userService: UsersService,
      private authService: AuthService,
      private router: Router,
      private route: ActivatedRoute,
      private title: Title,
      private meta: Meta
      ) {
      title.setTitle('Вход в систему');
      meta.addTags([
          {name: 'keywords', content: 'логин,вход,система'},
          {name: 'description', content: 'Страница входа в систему'}
      ]);
  }

  private showMessage(text: string, type: string = 'danger') {
      this.message = new Message(type, text);

      window.setTimeout(() => {
          this.message.text = '';
      }, 5000);
  }

  ngOnInit() {
      this.message = new Message('danger', '');
      this.route.queryParams
          .subscribe((params: Params) => {
              if (params['nowCanLogin']) {
                  this.showMessage('Теперь Вы можете зайти в систему', 'success');
              } else if (params['accessDenied']) {
                  this.showMessage('Авторизуйтесь для работы с системой!', 'warning');
              }
          });
    this.form = new FormGroup({
        'email': new FormControl(null, [Validators.required, Validators.email]),
        'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }

    onSubmit() {
      const formData = this.form.value;
      this.userService.getUserByEmail(formData.email)
          .subscribe((user: User) => {
            if (user) {
              if (user.password === formData.password) {
                  this.message.text = '';
                  window.localStorage.setItem('user', JSON.stringify(user));
                  this.authService.login();
                  this.router.navigate(['/system', 'bill']);
              } else {
                  this.showMessage('Пароль не верный!');
              }
            } else {
                this.showMessage('Такого пользователя не существует!');
            }
          });
    }
}
